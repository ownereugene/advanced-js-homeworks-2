const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];

const root = document.querySelector("#root");
root.innerHTML = `<ul id="booksList"></ul>`;
const booksList = document.querySelector("#booksList");

books.forEach((book) => {
    let status = 0;
    try {
        if (book.author)  {
            status += 1;
        }
        else {
            throw new Error("Book has not author");

        }
        if (book.name) {
            status += 1;
        }
        else {
            throw new Error("Book has not name");
        }
        if (book.price) {
            status +=1;
        }
        else {
            throw new Error("Price has not book");

        }
    } catch (error) {
        console.error(error);
    }
    if (status == 3){
        booksList.innerHTML += `
        <li>
            <p>${book.author}</p>
            <p>${book.name}</p>
            <p>${book.price}</p>
        </li>
        `
    }
})
